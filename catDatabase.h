/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file catDatabase.h
/// @version 1.0
///
/// @author David Paco <dpaco@hawaii.edu>
/// @date 10_Mar_2022
////////////////////////////////////
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#pragma once

#define MAX_NAME_LENGTH 32
#define MAX_CATS 10

// Declare Enums for Gender and Breed
enum gender {UNKNOWN_GENDER, MALE, FEMALE};
enum breed {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};

// Global Variables
extern char nameData[MAX_CATS][MAX_NAME_LENGTH];
extern enum gender genderData[MAX_CATS];
extern enum breed breedData[MAX_CATS];
extern bool boolData[MAX_CATS];
extern float weightData[MAX_CATS];
extern int CURRENT_CATS;

// Conversion from 5 Arrays to 1 Array of a Structure

struct catData
{
   char nameData[MAX_NAME_LENGTH];
   enum gender genderData;
   enum breed breedData;
   bool boolData;
   float weightData;
};

