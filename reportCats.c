/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file reportCats.c
/// @version 1.0
///
/// @author David Paco <dpaco@hawaii.edu>
/// @date 10_Mar_2022
////////////////////////////////////
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "catDatabase.h"
#include "reportCats.h"
#include "config.h"

int printCat(int index){
   // Check if index is less than 0 or more than number of cats in database
   if (index > MAX_CATS || index < 0){
      fprintf( stderr, "%s: Bad cat [%d]\n", PROGRAM_NAME, index);
      return 0;
      }
   else {
      printf("cat index = [%u] | name = [%s] | gender = [%d] | breed = [%d] | isFixed = [%d] | weight = [%f]\n", index, nameData[index], genderData[index], breedData[index], boolData[index], weightData[index]);
      }
   return 1;
}

int printAllCats(void){
   // Print all cats by stepping up from i
   for (int i = 0; i < CURRENT_CATS; ++i){
      printf("cat index = [%u] | name = [%s] | gender = [%d] | breed = [%d] | isFixed = [%d] | weight = [%f]\n", i, nameData[i], genderData[i], breedData[i], boolData[i], weightData[i]);  
   }
   return 1;
}

int findCat(char name[]){
   // Scan for name in database
   for (int i = 0; i < MAX_CATS; ++i){
      if (strcmp(name, nameData[i]) == 0){
         printf("Cat %s is in index %u\n", nameData[i], i);
         return i;
      }
   // Returns index number if found
   }
   return 0; // Returns error if not found
}
