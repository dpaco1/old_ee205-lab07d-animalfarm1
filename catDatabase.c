/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file catDatabase.c
/// @version 1.0
///
/// @author David Paco <dpaco@hawaii.edu>
/// @date 10_Mar_2022
////////////////////////////////////

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "catDatabase.h"
#include "config.h"

char nameData[MAX_CATS][MAX_NAME_LENGTH];
enum gender genderData[MAX_CATS];
enum breed breedData[MAX_CATS];
bool boolData[MAX_CATS];
float weightData[MAX_CATS];

// Current Number of Cats initialized to 0
int CURRENT_CATS = 0;

// Declare an array of the structure
struct catData arr_catData[10];
